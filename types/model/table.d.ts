export interface VTableHeader{
    field: string,
    label: string,
    maskData: Array<{}>,
    sortable: boolean,
    isHidden: boolean
}