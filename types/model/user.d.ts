export interface User {
    id: Number,
    full_name: String,
    birthday: Date,
    address: String,
    phone: String,
    sex: Number,
    image: String,
    effective_date: Date,
    expire_date: Date,
    department: Number,
    skill_tags: String,
    type: Number,
    note: String,
    created_at: Date,
    updated_at: Date,
    deleted_at: Date
}