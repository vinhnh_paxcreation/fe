import Vue from 'vue'
import * as icons from 'vue-feather-icons'
import { onGlobalSetup, provide } from '@nuxtjs/composition-api'
import cash from "cash-dom"

for (const [key, icon] of Object.entries(icons)) {
    Vue.component(key, icon)
}

export default ({}, inject: any) => {
    onGlobalSetup(() => {
      provide('cash', cash)
  
      // User config
      provide('sexs', [
        {value: 1, text: '男性'},
        {value: 2, text: '女性'}
      ])
  
      provide('departments', [
        {value: 1, text: 'Web'},
        {value: 2, text: 'iOS'},
        {value: 3, text: 'Android'}
      ])
  
      provide('types', [
        {value: 1, text: '社員'},
        {value: 2, text: '派遣'},
        {value: 3, text: 'フリーランス'},
        {value: 4, text: 'その他'}
      ]);
    })
  }