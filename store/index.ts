import { GetterTree, ActionTree, MutationTree } from 'vuex'
import axios from 'axios'
import appConst from '@/constants'
import { User } from '~/types/model'

export const state = () => ({
    loadedUsers: [] as User[]
})
  
export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, RootState> = {
    getLoadedUsers : (state) => {
        return state.loadedUsers
    }
}

export const mutations: MutationTree<RootState> = {
    SET_USERS: (state, users: User[]) => {
        state.loadedUsers = users
    },
    ADD_USER: (state, user: User) => {
        state.loadedUsers.push(user)
    },
    REMOVE_USER: (state, userId) => {
        const userIndex = state.loadedUsers.findIndex(
            user => user.id === userId
        );
        state.loadedUsers.splice(userIndex, 1)
    }
}

export const actions: ActionTree<RootState, RootState> = {
    async nuxtServerInit({ commit }) {
        return await axios.get('/api/users')
        .then(res => {
            if (res && res.data)
            {
                commit('SET_USERS', res.data)
            }
        })
        .catch(e => console.log(e))
    },
    async loadUsers({ commit }) {
        return await axios.get('/api/users')
        .then(res => {
            if (res && res.data)
            {
                commit('SET_USERS', res.data)
            }
        })
        .catch(e => console.log(e))
    },
    removeUser({commit}, userId: Number) {
        commit('REMOVE_USER', userId)
    },
    addUser({commit}, user: User) {
        commit('ADD_USER', user)
    }
}